import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PaginasRoutingModule } from './paginas/paginas.routing';

import { LoginComponent } from './auth/login/login.component';
import { RestorePasswordComponent } from './auth/restore-password/restore-password.component';

import { PingComponent } from './paginas/ping/ping.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'restorepassword', component: RestorePasswordComponent },
  { path: 'ping', component: PingComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    PaginasRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
