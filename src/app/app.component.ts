import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';

const base_url = environment.base_url;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sade-infra-v3';

  public salida: String = 'Un texto';
  constructor(){
    this.salida = base_url;
  }
}
