import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { UsuariosService } from 'src/app/servicios/usuarios.service';



@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements OnInit {

  verAdmin: boolean = false;
  verInfra: boolean = false;

  constructor(
    private _usuarios: UsuariosService
  ) { }

  ngOnInit(): void {

    if( this._usuarios.usuario.role === 'ADMIN' ){
      this.verAdmin = true;
    }


    if( this._usuarios.usuario.role === 'INFRA' ){
      this.verInfra = true;
    }

  }

}
