import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Usuario } from 'src/app/modelos/usuario.model';

import { UsuariosService } from 'src/app/servicios/usuarios.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent implements OnInit {

  usuario: Usuario = this._usuarios.usuario;

  constructor(
    private router: Router,
    private _usuarios: UsuariosService
  ) { }

  ngOnInit(): void {
  }

  salir(){
    localStorage.removeItem( 'token' );
    this.router.navigateByUrl('/login');
  }
}
