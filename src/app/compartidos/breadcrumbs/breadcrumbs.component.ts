import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';


@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styles: [
  ]
})
export class BreadcrumbsComponent implements OnInit {

  public titulo: string;
  public breadcrumb: string;

  public breadcrumbs$: Subscription;

  constructor(
    private router: Router
  ) { 

    this.breadcrumbs$ = this.getArgumentosRuta()
                            .subscribe(
                              data => {
                                this.breadcrumb = data.breadcrumb
                                this.titulo = data.titulo
                                document.title = ` SadeInfra - ${ data.titulo }`
                              }
                            );

  }

  ngOnInit(): void {
  }

  ngOnDestroy(){
    this.breadcrumbs$.unsubscribe();
  }

  getArgumentosRuta(){

    return this.router.events
      .pipe(
        filter( event => event instanceof ActivationEnd ), //obtengo solo el evento ActivationEnd
        filter( ( event: ActivationEnd ) => event.snapshot.firstChild === null ),
        map( ( event: ActivationEnd ) => event.snapshot.data )
      );

  }

}
