
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styles: [
  ]
})
export class AdministracionComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  listarUsuarios(){
    this.router.navigateByUrl( '/dashboard/usuarios/listar' );
  }

  crearUsuarios(){
    this.router.navigateByUrl( '/dashboard/usuarios/crear' );
  }

}
