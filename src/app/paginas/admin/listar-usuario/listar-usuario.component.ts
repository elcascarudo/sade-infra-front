import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/modelos/usuario.model';
import Swal from 'sweetalert2';

import { environment } from 'src/environments/environment';

import { BusquedasService } from 'src/app/servicios/busquedas.service';
import { UsuariosService } from 'src/app/servicios/usuarios.service';

@Component({
  selector: 'app-listar-usuario',
  templateUrl: './listar-usuario.component.html',
  styles: [
  ]
})

export class ListarUsuarioComponent implements OnInit {

  listaUsuarios: Usuario[] = [];
  listaUsuariosTemp: Usuario[] = []; // Mantengo una referencia temporal para cuando utilizo el buscador
  usuario: Usuario;
  total: number = 0;
  desde: number = 0;



  constructor(
    private _usuarios: UsuariosService,
    private _buscar: BusquedasService
  ) { 
    this.usuario = _usuarios.usuario;

  } 
  
  
  ngOnInit(): void {
    this.obtenerRegistros();
  }

  // Muestro la imagen de perfil
  fotoPerfil( img: string ){
    return `${ environment.base_url }/upload/foto/ver/${ img }`;
  }

  navegarRegistros( valor ){
    
    this.obtenerRegistros();

    this.desde += valor;

    if ( this.desde < 0 ) {
      this.desde = 0;
    } else if ( this.desde > this.total ) {
      this.desde -= valor;
    }
    
  }
  
  

  private obtenerRegistros( ){
    this._usuarios.listarUsuarios( this.desde ).subscribe( ( resp: any ) => {
      this.listaUsuarios = resp.usuarios;
      this.listaUsuariosTemp = resp.usuarios;
      this.total = resp.total;
    });

  }


  buscar( termino: string ){
    
    if( termino.length === 0 ){
      return this.listaUsuarios = this.listaUsuariosTemp;
    }

    this._buscar.buscarUsuario( termino ).subscribe( (resp: any )=> {
      this.listaUsuarios = resp.usuarios;
    });
    
  }


  eliminar( uid: string ){

    if( this.usuario.uid === uid ){
      return Swal.fire( 'Error', 'No te podes borrar porque haces macana!!', 'error' );
    }
    
    this._usuarios.bloquearUsuarios( uid ).subscribe( (resp: any) => {
      Swal.fire( 'Cambio realizado correctamente', resp.msg, 'success' );
      this.obtenerRegistros();
    });
    
  }

  cambiarRole( detalle: Usuario ){
    
    this._usuarios.actualizarUsuarios( detalle )
                  .subscribe( resp => Swal.fire( 'Actualización realizada', `Se actualizo el rol del usuario "${ detalle.nombre }"`, 'success' ) );
  }

}
