import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { ListarUsuarioComponent } from './listar-usuario/listar-usuario.component';
import { AdministracionComponent } from './administracion/administracion.component';



@NgModule({
  declarations: [
    CrearUsuarioComponent,
    ListarUsuarioComponent,
    AdministracionComponent
  ],
  exports: [
    CrearUsuarioComponent,
    ListarUsuarioComponent,
    AdministracionComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule, 
    ReactiveFormsModule
  ]
})

export class AdminModule { }
