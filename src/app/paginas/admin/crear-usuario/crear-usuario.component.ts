import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/servicios/usuarios.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styles: [
  ]
})
export class CrearUsuarioComponent implements OnInit {

  public usuariosForm = this.fb.group({
     nombre:    [ '',         [ Validators.required, Validators.minLength( 5 ) ] ],
     email:     [ '',         [ Validators.required, Validators.email ] ],
     password:  [ 'Test1234', [ Validators.required, Validators.minLength( 8 ) ] ],
     role:      [ 'DESA',     [ Validators.required ] ]
  });

  constructor(
    private fb: FormBuilder,
    private _usuarios: UsuariosService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  guardarUsuario(){
    
    

    if( this.usuariosForm.status === 'INVALID' ){
      Swal.fire( 'Todos los campos deben ser completados','El nombre debe temer mas de 5 caracteres. <br> El mail debe tener formato valido. <br> La contraseña debe tener mas de 8 caracters', 'error' );
    }

    this._usuarios.crearUsuario( this.usuariosForm.value ).subscribe( ( resp: any ) => {
      Swal.fire( `Usuario creado correctamente`, `Usuarios: <b> ${ resp.usuario.email } </b> <br> Contraseña: <b> ${ this.usuariosForm.value.password } </b> `, 'success' )
      this.usuariosForm.reset();
      this.router.navigateByUrl( '/dashboard/admin' );
    });
    
  }

}
