import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import { BbddService } from 'src/app/servicios/bbdd.service';

@Component({
  selector: 'app-crear-bases-de-datos',
  templateUrl: './crear-bases-de-datos.component.html',
  styles: [
  ]
})
export class CrearBasesDeDatosComponent implements OnInit {

  ver: boolean = false;

  public bbddForm = this.fb.group({
    ambiente:  [ 'CIUDAD', [ Validators.required, Validators.minLength( 3 ) ] ],
    entorno:   [ 'PROD',   [ Validators.required, Validators.minLength( 2 ) ] ],
    host:      [ '',       [ Validators.required ] ],
    puerto:    [ '1521',   [ Validators.required, Validators.minLength( 2 ) ] ],
    servicio:  [ '',       [ Validators.required, Validators.minLength( 5 ) ] ]
  });

  constructor(
    private fb: FormBuilder,
    private _bbdd: BbddService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }


  guardarBBDD(){

    if( this.bbddForm.status === "INVALID" ){
      Swal.fire( 'Error', 'Todos los campos deben ser competados', 'error' );
      return;
    }


    this._bbdd.crearBBDD( this.bbddForm.value ).subscribe(
      ( resp: any )=> {
        Swal.fire( 'Se agrego BBDD', `La BBDD ${ resp.database.host }:${ resp.database.puerto }/${ resp.database.servicio } se agrego correctamente`, 'success' );
        this.router.navigateByUrl( '/dashboard/databases' );
      },
      ( error ) => {
        Swal.fire( 'Error al guardar BBDD', `${ error.error.msg  }`, 'error' );        
      }
    )
    
  }
}
