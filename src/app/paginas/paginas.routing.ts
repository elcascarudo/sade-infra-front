import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../guards/auth.guard';
import { EsAdminGuard } from '../guards/es-admin.guard';
import { EsInfraGuard } from '../guards/es-infra.guard';
import { EsInfraAdminGuard } from '../guards/es-infra-admin.guard';

import { PaginasComponent } from './paginas.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BasesDeDatosComponent } from './bases-de-datos/bases-de-datos.component';
import { ServidoresClientesComponent } from './servidores-clientes/servidores-clientes.component';
import { ServidoresEverisComponent } from './servidores-everis/servidores-everis.component';
import { ModulosEverisComponent } from './modulos-everis/modulos-everis.component';

import { ListarUsuarioComponent } from './admin/listar-usuario/listar-usuario.component';
import { CrearUsuarioComponent } from './admin/crear-usuario/crear-usuario.component';
import { AdministracionComponent } from './admin/administracion/administracion.component';
import { ListarAccesosComponent } from './listar-accesos/listar-accesos.component';
import { CrearAccesosComponent } from './crear-accesos/crear-accesos.component';
import { PerfilComponent } from './perfil/perfil.component';
import { CrearServidorClienteComponent } from './crear-servidor-cliente/crear-servidor-cliente.component';
import { CrearBasesDeDatosComponent } from './crear-bases-de-datos/crear-bases-de-datos.component';
import { DetallesBasesDeDatosComponent } from './detalles-bases-de-datos/detalles-bases-de-datos.component';






const routes: Routes = [
  { 
    path: 'dashboard', 
    component: PaginasComponent,
    canActivate: [ AuthGuard ],
    children: [
      { path: '', component: DashboardComponent, 
        data: { 
                titulo: 'Dashboard',
                breadcrumb: [ 'Dashboard' ] 
              } 
      },

      { path: 'databases', component: BasesDeDatosComponent, 
        data: { 
                titulo: 'Base de Datos',
                breadcrumb: [ 'Dashboard', 'Base de Datos'] 
              } 
      },

      { path: 'databases/detalle/:id', component: DetallesBasesDeDatosComponent, 
        data: { 
                titulo: 'Detalles de la Base de Datos',
                breadcrumb: [ 'Dashboard', 'Base de Datos', 'detalles'] 
              } 
      },

      { path: 'clientes/servidores', component: ServidoresClientesComponent, 
        data: { 
                titulo: 'Servidores Clientes',
                breadcrumb: [ 'Dashboard', 'clientes', 'Servidores'] 
              } 
      },

      { path: 'everis/servidores', component: ServidoresEverisComponent, 
        data: { 
                titulo: 'Servidores Everis',
                breadcrumb: [ 'Dashboard', 'Everis', 'Servidores'] 
              } 
      },

      { path: 'everis/modulos', component: ModulosEverisComponent, 
        data: { 
                titulo: 'Módulos entorno test',
                breadcrumb: [ 'Dashboard', 'Everis', 'Módulos'] 
              } 
      },

      { path: 'perfil', component: PerfilComponent, 
        data: { 
                titulo: 'Detalles de perfil',
                breadcrumb: [ 'Dashboard', 'perfil' ] 
              } 
      },

      // Módulos administrativos
      { path: 'usuarios/listar', component: ListarUsuarioComponent, 
        canActivate: [ EsAdminGuard ],
        data: { 
                titulo: 'Listar usuarios',
                breadcrumb: [ 'Dashboard', 'Administración', 'Listar Usuarios'] 
              } 
      },

      { path: 'usuarios/crear', component: CrearUsuarioComponent, 
        canActivate: [ EsAdminGuard ],
        data: { 
                titulo: 'Crear usuario',
                breadcrumb: [ 'Dashboard', 'Administración', 'Crear Usuarios'] 
              } 
      },

      { path: 'admin', component: AdministracionComponent, 
        canActivate: [ EsAdminGuard ],
        data: { 
                titulo: 'Administración',
                breadcrumb: [ 'Dashboard', 'Administración' ] 
              } 
      },


      { path: 'accesos', component: ListarAccesosComponent, 
        canActivate: [ EsInfraGuard ],
        data: { 
                titulo: 'Lista de accesos rapidos',
                breadcrumb: [ 'Dashboard', 'accesos', 'listar' ] 
              } 
      },


      { path: 'accesos/crear', component: CrearAccesosComponent, 
        canActivate: [ EsInfraGuard ],
        data: { 
                titulo: 'Crear accesos rapidos',
                breadcrumb: [ 'Dashboard', 'accesos', 'crear' ] 
              } 
      },

      { path: 'databases/crear', component: CrearBasesDeDatosComponent, 
        canActivate: [ EsInfraGuard ],
        data: { 
                titulo: 'Crear nueva Base de Datos',
                breadcrumb: [ 'Dashboard', 'Base de Datos', 'crear'] 
              } 
      },

      { path: 'clientes/servidores/crear', component: CrearServidorClienteComponent, 
        canActivate: [ EsInfraAdminGuard ],
        data: { 
                titulo: 'Crear nuevo servidor clientes',
                breadcrumb: [ 'Dashboard', 'clientes', 'Servidores', 'crear'] 
              } 
      },

      

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaginasRoutingModule { }