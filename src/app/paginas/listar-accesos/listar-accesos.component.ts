import { Component, OnInit } from '@angular/core';
import { AccesosRapidos } from 'src/app/modelos/accesos-rapidos.model';

import { AccesosRapidosService } from 'src/app/servicios/accesos-rapidos.service';

@Component({
  selector: 'app-listar-accesos',
  templateUrl: './listar-accesos.component.html'
})
export class ListarAccesosComponent implements OnInit {

  mostrarDetalle: boolean = false;
  detalle: AccesosRapidos;

  accesos: AccesosRapidos[] = [];

  constructor(
    private _accesos: AccesosRapidosService
  ) { }

  ngOnInit(): void {
    this.listarAcessosRapidos();

  }
  
  
  
  detalleAcceso( id: string ){
    this.verDetalle( id );
  }

  
  eliminarAcceso( ){
    
  }
  
  private listarAcessosRapidos(){

    this._accesos.listarAccesosRapidos().subscribe( ( resp: any ) => {
      this.accesos = resp.accesos;
    });
    
  }

  private verDetalle( id: string ){

    this._accesos.detalleAccesoRapido( id ).subscribe( ( resp: any ) => {
      this.detalle = resp.acceso[0] ;
      console.log( this.detalle); 
      
      this.mostrarDetalle = true;
    });

  }


}
