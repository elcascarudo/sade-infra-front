import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

import { ServidoresService } from 'src/app/servicios/servidores.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-crear-servidor-cliente',
  templateUrl: './crear-servidor-cliente.component.html',
  styles: [
  ]
})
export class CrearServidorClienteComponent implements OnInit {

  public servidorForm = this.fb.group({
    ambiente:     [ 'CIUDAD', [ Validators.required, Validators.minLength( 3 ) ] ],
    entorno:      [ 'PROD',   [ Validators.required, Validators.minLength( 2 ) ] ],
    desarrollo:   [ 'GDE',    [ Validators.required ] ],
    modulo:       [ '',       [ Validators.required, Validators.minLength( 2 ) ] ],
    tipo:         [ '' ],
    nodo:         [ 'N1',     [ Validators.required ] ],
    ip:           [ '',       [ Validators.required, Validators.minLength( 3 ) ] ]
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _servidores: ServidoresService
  ) { }

  ngOnInit(): void {
  }

  guardarServidor(){

    if( this.servidorForm.status === "INVALID" ){
      Swal.fire( 'Error', 'Solo el campo "Tipo" no es requerido', 'error' );
      return;
    }

    // Guardo los datos en la BBDD
    this._servidores.crearServidorCliente( this.servidorForm.value ).subscribe( ( resp: any ) => {
      Swal.fire({
        position: 'center',
        icon: 'success',
        html: `Se guardo correctamente el servidor ${ resp.servidor.modulo } ${ resp.servidor.tipo } ${ resp.servidor.nodo } correctamente`,
        showConfirmButton: false,
        timer: 2000
      });


      this.router.navigateByUrl( '/dashboard/clientes/servidores' );
    });
    
  }
}
