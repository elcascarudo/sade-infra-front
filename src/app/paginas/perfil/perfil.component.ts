import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/modelos/usuario.model';
import { FileUploadService } from 'src/app/servicios/file-upload.service';
import { UsuariosService } from 'src/app/servicios/usuarios.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: [
  ]
})
export class PerfilComponent implements OnInit {

  public perfilForm: FormGroup;
  public usuario: Usuario;
  public imagenSubir: File;

  public imgTemp: any = '';

  constructor(
    private fb: FormBuilder,
    private _usuario: UsuariosService,
    private _fileUpload: FileUploadService
  ) { 
    this.usuario = _usuario.usuario
  }

  ngOnInit(): void {



  }

  cambiarImagen( file: File ){
    
    this.imagenSubir = file;

    if( !file ) {
      return this.imgTemp = null;
    }

    const reader = new FileReader();
    reader.readAsDataURL( file );

    reader.onloadend = () => {
      this.imgTemp = reader.result ;     
    }
  }

  subirImagen(){
    this._fileUpload
        .actualizarFoto( this.imagenSubir, this.usuario.uid )
        .then( img => this.usuario.img = img )
      }
    
}