import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';


import { AccesosRapidosService } from 'src/app/servicios/accesos-rapidos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-accesos',
  templateUrl: './crear-accesos.component.html',
  styles: [
  ]
})
export class CrearAccesosComponent implements OnInit {

  public accesoForm = this.fb.group({
    nombre:   [ '', [ Validators.required, Validators.minLength( 3 ) ] ],
    usuario:  [ ' ' ],
    password: [ ' ' ],
    url:      [ '', [ Validators.required, Validators.minLength( 3 ) ] ]
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _accesosRapidos: AccesosRapidosService
  ) { }

  ngOnInit(): void {
  }


  guardarAcceso(){

    if( this.accesoForm.status === "INVALID" ){
      Swal.fire( 'Error', 'Todos los campos deben ser competados', 'error' );
      return;
    }


    // Guardo los datos en la BBDD
    this._accesosRapidos.crearAccesoRapidos( this.accesoForm.value ).subscribe( ( resp: any ) => {
      Swal.fire({
        position: 'center',
        icon: 'success',
        html: `Se guardo correctamente el acceso a ${ resp.acceso.nombre }  correctamente`,
        showConfirmButton: false,
        timer: 2000
      });

      console.log( resp );
      

      this.router.navigateByUrl( '/dashboard/accesos' );
    });
    
  }
}
