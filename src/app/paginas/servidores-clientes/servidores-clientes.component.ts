import { Component, OnInit } from '@angular/core';
import { ServidoresService } from 'src/app/servicios/servidores.service';
import { UsuariosService } from 'src/app/servicios/usuarios.service';


declare function initTableFuction(): any;

@Component({
  selector: 'app-servidores-clientes',
  templateUrl: './servidores-clientes.component.html',
  styles: [
  ]
})
export class ServidoresClientesComponent implements OnInit {

  esInfra: boolean = false;

  titulos: any = [
    "Ambiente",
    "Entorno",
    "Desarrollo",
    "Módulo",
    "IP"
  ]
  
  campos: any = [];

  constructor(
    private _usuario: UsuariosService,
    private _servidores: ServidoresService
  ) { }

  ngOnInit(): void {
    
    this._usuario.usuario.role === 'INFRA' ? this.esInfra = true : this.esInfra = false;
    
    this.listarServidoresClientes();
  }
  
  
  listarServidoresClientes(){
    
    this._servidores.listarServidoresClientes().subscribe( ( resp: any ) => {
      this.campos = resp.servidores;
      // Cargo la configuración de la tabla que se encuentra en el index.html
      initTableFuction();
    });
    
  }

}
