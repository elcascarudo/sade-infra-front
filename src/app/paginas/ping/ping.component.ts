import { Component, OnInit } from '@angular/core';
import { PingService } from 'src/app/servicios/ping.service';
import { environment } from 'src/environments/environment';



@Component({
  selector: 'app-ping',
  templateUrl: './ping.component.html',
  styles: [
  ]
})
export class PingComponent implements OnInit {

  msg: string = 'No llego la pelota';
  base_url: string = environment.base_url;

  constructor(
    private _ping: PingService
  ) { }

  ngOnInit(): void {

    this._ping.ping().subscribe( ( resp: any ) => {
      this.msg = resp.msg;
    });

  }

}
