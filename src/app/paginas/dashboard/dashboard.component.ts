import { Component, OnInit } from '@angular/core';
import { UsuariosService } from 'src/app/servicios/usuarios.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: [
  ]
})
export class DashboardComponent implements OnInit {

  esInfra: boolean = false;

  constructor(
    private _usuario: UsuariosService
  ) { 

  }

  ngOnInit(): void {
    this._usuario.usuario.role === 'INFRA' ? this.esInfra = true : this.esInfra = false;
  }

}
