import { Component, OnInit } from '@angular/core';

import { BbddService } from 'src/app/servicios/bbdd.service';
import { UsuariosService } from 'src/app/servicios/usuarios.service';

declare function initTableFuction(): any;

@Component({
  selector: 'app-bases-de-datos',
  templateUrl: './bases-de-datos.component.html',
  styles: [
  ]
})
export class BasesDeDatosComponent implements OnInit {

  esInfra: boolean = false;

  titulos: any = [
    "ambiente",
    "entorno",
    "host",
    "puerto",
    "servicio"
  ]
  
  campos: any[] = [];

  constructor(
    private _bbdd: BbddService,
    private _usuario: UsuariosService
  ) { }

  ngOnInit(): void {
    this._usuario.usuario.role === 'INFRA' ? this.esInfra = true : this.esInfra = false;
    this.getBBDD();
  }

  detalle( id: string ){
    console.log( id ); 
  }

  private getBBDD(){
    this._bbdd.getBBDD().subscribe( (resp: any) => {
      this.campos = resp.database;
      initTableFuction();
    });
  }

}
