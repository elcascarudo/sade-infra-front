import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataBases } from 'src/app/modelos/bbdd.model';
import { BbddService } from 'src/app/servicios/bbdd.service';
import { PasswordsService } from 'src/app/servicios/passwords.service';
import { UsuariosService } from 'src/app/servicios/usuarios.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detalles-bases-de-datos',
  templateUrl: './detalles-bases-de-datos.component.html',
  styles: [
  ]
})
export class DetallesBasesDeDatosComponent implements OnInit {

  esInfra: boolean = false;
  
  private id: string;
  detalleBBDD: DataBases;

  passwords: any = [];

  userNominales: any = [];
  userEspemas: any = [];


  public passwordForm = this.fb.group({
    tipo:   [ '', [ Validators.required, Validators.minLength( 3 ) ] ],
    usuario:  [ ' ', [ Validators.required, Validators.minLength( 3 ) ] ],
    password: [ ' ', [ Validators.required, Validators.minLength( 3 ) ] ]
  })

  constructor(
    private parametros: ActivatedRoute,
    private _bbdd: BbddService,
    private _usuario: UsuariosService,
    private _passsword: PasswordsService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {

    this.id = this.parametros.snapshot.params.id;

    this.detalle( this.id );
    this.consultarPasswords()

    this._usuario.usuario.role === 'INFRA' ? this.esInfra = true : this.esInfra = false;
    
  }


  detalle( id: string ){
    this._bbdd.detalle( id ).subscribe( ( resp: any ) => this.detalleBBDD = resp.database );
  }

  eliminar( id: string ){
    
    this._bbdd.eliminar( id ).subscribe( ( resp: any ) => { 

      // elimino todos las contaseñas relacionadas
      this.passwords.forEach( users => {
        this.eliminarUsuario( users.id );
      });

      Swal.fire( 
        'BBDD Eliminada', 
        `La BBDD  <br> ${ resp.database.host }:${ resp.database.puerto }/${ resp.database.servicio } <br> se elimino correctamente`, 
        'success' );

      this.router.navigateByUrl( '/dashboard/databases' );
    });


  }


  guardarPassword(){

    const password = {
      ...this.passwordForm.value,
      relacion: this.id
    }

    this._passsword.crear( password ).subscribe( resp => {  
      this.consultarPasswords();
      this.passwordForm.reset();
    });


    
    
  }

  consultarPasswords(){
    this._passsword.listar( this.id ).subscribe( ( resp: any ) => {
    
      this.passwords = resp.passwords;
      this.userEspemas = [];
      this.userNominales = [];

      this.passwords.forEach( user => {
        
        if( user.tipo === "NOMINAL" ){
          this.userNominales.push( user );
        }

        if( user.tipo === "ESQUEMA" ){
          this.userEspemas.push( user );
        }

      });
    
    });
  }

  eliminarUsuario( id: string ){
    this._passsword.borrar( id ).subscribe( resp => this.consultarPasswords() );
  }

}
