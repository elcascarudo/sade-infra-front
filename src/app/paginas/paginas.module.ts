import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CompartidosModule } from '../compartidos/compartidos.module';

import { PaginasComponent } from './paginas.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ServidoresClientesComponent } from './servidores-clientes/servidores-clientes.component';
import { BasesDeDatosComponent } from './bases-de-datos/bases-de-datos.component';
import { ServidoresEverisComponent } from './servidores-everis/servidores-everis.component';
import { ModulosEverisComponent } from './modulos-everis/modulos-everis.component';
import { CrearBasesDeDatosComponent } from './crear-bases-de-datos/crear-bases-de-datos.component';
import { DetallesBasesDeDatosComponent } from './detalles-bases-de-datos/detalles-bases-de-datos.component';
import { ListarAccesosComponent } from './listar-accesos/listar-accesos.component';
import { CrearAccesosComponent } from './crear-accesos/crear-accesos.component';
import { PerfilComponent } from './perfil/perfil.component';
import { AdminModule } from './admin/admin.module';
import { CrearServidorClienteComponent } from './crear-servidor-cliente/crear-servidor-cliente.component';
import { CrearServidorEverisComponent } from './crear-servidor-everis/crear-servidor-everis.component';
import { PingComponent } from './ping/ping.component';



@NgModule({
  declarations: [
    PaginasComponent,
    DashboardComponent,
    ServidoresClientesComponent,
    BasesDeDatosComponent,
    ServidoresEverisComponent,
    ModulosEverisComponent,
    CrearBasesDeDatosComponent,
    DetallesBasesDeDatosComponent,
    ListarAccesosComponent,
    CrearAccesosComponent,
    PerfilComponent,
    CrearServidorClienteComponent,
    CrearServidorEverisComponent,
    PingComponent
  ],
  exports: [
    PaginasComponent,
    DashboardComponent,
    ServidoresClientesComponent,
    BasesDeDatosComponent,
    ServidoresEverisComponent,
    ModulosEverisComponent,
    CrearBasesDeDatosComponent,
    DetallesBasesDeDatosComponent,
    ListarAccesosComponent,
    CrearAccesosComponent,
    PerfilComponent,
    CrearServidorClienteComponent,
    CrearServidorEverisComponent,
    PingComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    CompartidosModule,
    AdminModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PaginasModule { }
