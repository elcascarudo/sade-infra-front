export class ServidorClientes {

  constructor(
    public ambiente: string,
    public entorno: string,
    public desarrollo: string,
    public modulo: string,
    public nodo: string,
    public ip: string,
    public tipo?: string,
  ) {}

}