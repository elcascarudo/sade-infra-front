import { environment } from "src/environments/environment";

export class Usuario {

  constructor(
    public nombre: string,
    public email: string,
    public password?: string,
    public img?: string,
    public role?: string,
    public uid?: string,
  ) {}

  get verFotoPerfil() {
    if ( this.img ) {
      return `${ environment.base_url }/upload/foto/ver/${ this.img }`;
    } else {
      return `${ environment.base_url }/upload/foto/ver/no-image`;
    }
  }
}