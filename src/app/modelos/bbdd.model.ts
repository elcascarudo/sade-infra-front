export class DataBases {

  constructor(
    public ambiente: string,
    public entorno: string,
    public host: string,
    public puerto: string,
    public servicio: string,
  ) {}

}