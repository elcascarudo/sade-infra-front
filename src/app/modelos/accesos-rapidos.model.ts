export class AccesosRapidos {

  constructor(
    public nombre: string,
    public url: string,
    public usuario?: string,
    public password?: string,
  ) {}

}