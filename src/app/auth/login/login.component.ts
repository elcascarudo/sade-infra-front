import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

import { UsuariosService } from 'src/app/servicios/usuarios.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  public loginForm = this.fb.group({
    email:    [ localStorage.getItem( 'email' ) || '', [ Validators.required, Validators.email ] ],
    password: [ '',                                    [ Validators.required, Validators.minLength( 6 ) ] ],
    recordar: [ localStorage.getItem( 'recordar' ) || false ]
  });

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private _login: UsuariosService
  ) { }

  ngOnInit(): void {
  }

  login(){
    
    this._login.login( this.loginForm.value ).subscribe( ( resp: any ) => {
      
      // Guardo el token en el localStorage
      localStorage.setItem( 'token', resp.token );

      if ( this.loginForm.value.recordar ) {
        //Recurdo el mail si lo quiere el usuario en el localStorage
        localStorage.setItem( 'email', this.loginForm.value.email );
        localStorage.setItem( 'recordar', 'true' );
      } else {
        // Dejo de recordar el usuario :P
        localStorage.removeItem( 'email' );
        localStorage.removeItem( 'recordar' );
      }

      // Redirecciono al dashboard
      this.router.navigateByUrl( '/' );
    },
    err =>  Swal.fire( 'Error en los datos', err.error.msg, 'error' ) );
  }

  ingresar(){

    // localStorage.setItem( 'auth', 'true' );
    // this.router.navigateByUrl('/dashboard');

    let body = {
      "email": "elcascarudo2@gmail.com",
      "password": "123456"
    };


  }

}
