import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/servicios/usuarios.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-restore-password',
  templateUrl: './restore-password.component.html',
  styles: [
  ]
})
export class RestorePasswordComponent implements OnInit {

  public restoreForm = this.fb.group({
    email: [ '', [ Validators.required, Validators.email ] ]
  });

  constructor(
    private _usuarios: UsuariosService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  recuperarPassword(){
    
    if( !this.restoreForm.valid ){
      Swal.fire( 'Error', 'El mail debe tener un formato valido', 'error' );
      return;
    }
    
    Swal.fire( 'Enviando email a:', `${ this.restoreForm.value.email } ` );
    Swal.showLoading( );

    this._usuarios.restorePassword( this.restoreForm.value.email ).subscribe( ( resp: any ) => {
      
      Swal.close();

      if( resp.ok ){
        Swal.fire( 'Mail enviado correctamente', `${ resp.msg }`, 'success' );
        this.router.navigateByUrl( '/login' );
      } else {
        Swal.fire( 'Mail inexistente', `${ resp.msg }`, 'error' );
        this.restoreForm.reset();
      }
      
    });
    

  }


}
