import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UsuariosService } from '../servicios/usuarios.service';

@Injectable({
  providedIn: 'root'
})
export class EsInfraGuard implements CanActivate {

  constructor(
    private router: Router,
    private _usuarios: UsuariosService
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    
      if( this._usuarios.usuario.role === 'INFRA' ){
        return true;
      } else {
        this.router.navigateByUrl( '/dashboard' );
        return false;
      }

  }
  
}
