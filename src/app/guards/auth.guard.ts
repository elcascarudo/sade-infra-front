import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';



import { map, tap } from 'rxjs/operators';
import { UsuariosService } from '../servicios/usuarios.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private _usuario: UsuariosService,
    private router: Router
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

      
    return this._usuario.validarToken()
                        .pipe(
                          tap( estaAutenticado => {

                            if( !estaAutenticado )
                              this.router.navigateByUrl('/login');

                          })
                        );
  }
  
}
