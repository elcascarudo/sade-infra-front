import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { environment } from 'src/environments/environment';
import { Usuario } from '../modelos/usuario.model';
import { Router } from '@angular/router';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  public usuario: Usuario;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }



  login( body: { email: string, password: string }){    
    return this.http.post( `${ base_url }/login`, body);
  }


  validarToken(): Observable<boolean>{

    const token = localStorage.getItem( 'token' ) || '';


    return this.http.get( `${ base_url }/login/renew?token=${ token }` )
                    .pipe(
                      tap( (resp: any ) => {

                        const { email, nombre, role, img, uid } = resp.usuario;
                        // Creo un nuevo objeto para que quede refernciado
                        this.usuario = new Usuario( nombre, email, '', img, role, uid );
                        // Guardo el nuevo token generado
                        localStorage.setItem( 'token', resp.token );
                      }),
                      map( resp => true ),
                      catchError( resp => of( false ) ) // El "of" retorno un nuevo observable
                    );


  }


  esAdmin(): Observable<boolean>{

    const token = localStorage.getItem( 'token' ) || '';


    return this.http.get( `${ base_url }/login/renew?token=${ token }` )
                    .pipe(
                      tap( (resp: any ) => {

                        if( resp.usuario.role !== 'ADMIN' ){
                          return of( false );
                        }
                        
                      }),
                      map( resp => true ),
                      catchError( resp => of( false ) ) // El "of" retorno un nuevo observable
                    );


  }

  crearUsuario( data: Usuario ){

    return this.http.post( `${ base_url }/usuarios?token=${ this.token }`, data );

  }

  actualizarPerfil( data: { email: string, nombre: string } ){

    return this.http.put( `${ base_url }/usuarios/${ this.uid }?token=${ this.token }`, data );

  }


  actualizarUsuarios( data: Usuario ){

    return this.http.put( `${ base_url }/usuarios/${ data.uid }?token=${ this.token }`, data );

  }

  listarUsuarios( desde: number = 0 ){
    return this.http.get( `${ base_url }/usuarios?desde=${ desde }&token=${ this.token }` );
  }


  bloquearUsuarios( uid: string ){
    return this.http.delete( `${ base_url }/usuarios/${ uid }?token=${ this.token }` );
  }

  restorePassword( email: string ){
    return this.http.get( `${ base_url }/login/restorePassword/${ email }` );
  }

  /**********************************************************************************
   * getter's
   */

   get token(): string {
    return localStorage.getItem('token') || '';
  }

  get uid(): string {
    return this.usuario.uid || '';
  }
}
