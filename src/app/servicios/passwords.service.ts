import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


const base_url = environment.base_url

@Injectable({
  providedIn: 'root'
})
export class PasswordsService {

  constructor(
    private http: HttpClient
  ) { }


  crear( password ){
    return this.http.post( `${ base_url }/password?token=${ this.token }`, password );
  }

  listar( id: string ){
    return this.http.get( `${ base_url }/password/${ id }?token=${ this.token }` );
  }

  borrar( id: string ){
    return this.http.delete( `${ base_url }/password/${ id }?token=${ this.token }` );
  }

  /**********************************************************************************
   * getter's
   */

    get token(): string {
      return localStorage.getItem('token') || '';
    }
  
}
