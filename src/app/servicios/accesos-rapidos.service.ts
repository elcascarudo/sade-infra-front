import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { AccesosRapidos } from '../modelos/accesos-rapidos.model';

const base_url = environment.base_url;


@Injectable({
  providedIn: 'root'
})
export class AccesosRapidosService {


  constructor(
    private http: HttpClient
  ) { }


  crearAccesoRapidos( acceso: AccesosRapidos ){
    return this.http.post( `${ base_url }/accesos/rapidos?token=${ this.token }`, acceso );
  }

  listarAccesosRapidos(){

    return this.http.get( `${ base_url }/accesos/rapidos?token=${ this.token }`);
  }

  detalleAccesoRapido( id: String ){
    return this.http.get( `${ base_url }/accesos/rapidos/detalle/${ id }?token=${ this.token }`);
  }

  eliminarAccesoRapido( id: string ){
    return this.http.delete( `${ base_url }/accesos/rapidos/${ id }?token=${ this.token }`);
  }

  /**********************************************************************************
   * getter's
   */

  get token(): string {
    return localStorage.getItem('token') || '';
  }

}
