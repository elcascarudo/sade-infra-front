import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class BusquedasService {

  constructor(
    private http: HttpClient
  ) { }


  buscarUsuario( termino: string ){

    return this.http.get<any[]>( `${ base_url }/buscar/usuarios/${ termino }?token=${ this.token }` );
  }


  /**********************************************************************************
   * getter's
   */

   get token(): string {
    return localStorage.getItem('token') || '';
  }

}
