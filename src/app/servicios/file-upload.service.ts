import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Usuario } from '../modelos/usuario.model';
import { UsuariosService } from './usuarios.service';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  public usuario: Usuario;

  constructor(
    private _usuarios: UsuariosService
  ) { 
    this.usuario = _usuarios.usuario;
  }


  async actualizarFoto(
    archivo: File,
    id: string
  ){

    try {

      const token = localStorage.getItem( 'token' );
      const url = `${ base_url }/upload/foto/perfil/${ id }?token=${ token }`;

      const formData = new FormData();
      formData.append( 'imagen', archivo );

      const resp = await fetch( url, {
        method: 'PUT',
        body: formData
      });

      const data = await resp.json();

      if ( data.ok ) {
        return data.usuario.img;
      } else {
        return false;
      }

    } catch (error) {
      console.log( error );
      return false;
    }

  }
}
