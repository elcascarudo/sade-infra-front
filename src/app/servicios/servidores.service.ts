import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

import { ServidorClientes } from '../modelos/servidor-clientes.model';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class ServidoresService {

  constructor(
    private http: HttpClient
  ) { }



  crearServidorCliente( servidor: ServidorClientes){

    return this.http.post( `${ base_url }/servidores/clientes?token=${ this.token }`, servidor );
  }

  listarServidoresClientes(){

    return this.http.get( `${ base_url }/servidores/clientes?token=${ this.token }`);
  }

  

  /**********************************************************************************
 * getter's
 */

    get token(): string {
    return localStorage.getItem('token') || '';
  }


}
