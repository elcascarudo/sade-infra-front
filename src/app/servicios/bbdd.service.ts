import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';

import { DataBases } from '../modelos/bbdd.model';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class BbddService {

  constructor(
    private http: HttpClient
  ) { }




  getBBDD(){
    return this.http.get( `${ base_url }/database?token=${ this.token }` );
  }


  detalle( id: string ){
    return this.http.get( `${ base_url }/database/detalle/${ id }?token=${ this.token }` );
  }

  crearBBDD( bbdd: DataBases ){
    return this.http.post( `${ base_url }/database?token=${ this.token }`, bbdd );
  }

  eliminar( id: string ){
    return this.http.delete( `${ base_url }/database/${ id }?token=${ this.token }` );
  }


  get token(){
    return localStorage.getItem( 'token' );
  }
}
